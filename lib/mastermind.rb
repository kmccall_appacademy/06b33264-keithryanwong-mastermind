require 'byebug'

class Code
  PEGS = { r: true, g: true, y: true, b: true, o: true, p: true }

  attr_reader :pegs

  def self.parse(str)
    str.downcase!
    pegs = str.chars.map(&:to_sym)

    raise 'invalid colors' unless pegs.all? { |peg| PEGS[peg] }
    raise 'invalid length' if pegs.length != 4

    Code.new(pegs)
  end

  def self.random
    random_pegs = []
    until random_pegs.length == 4
      random_pegs.push(PEGS.keys.shuffle[0])
    end

    Code.new(random_pegs)
  end

  def initialize(pegs)
    @pegs = pegs #array of syms
  end

  def [](index)
    @pegs[index]
  end

  def exact_matches(code)
    matches = 0

    (0..3).each { |i| matches += 1 if code[i] == @pegs[i] }

    matches
  end

  def near_matches(code)
    total_matches(code) - exact_matches(code)
  end

  def total_matches(code)
    secret_code = @pegs.dup

    code.pegs.each do |peg|
      i = secret_code.index(peg)
      secret_code.delete_at(i) if i
    end

    4 - secret_code.length
  end

  def ==(code)
    code.is_a?(Code) && @pegs == code.pegs
  end
end

class Game
  NUMBER_OF_TURNS = 10

  attr_reader :secret_code
  attr_accessor :guesses

  def initialize(code = Code.random)
    @secret_code = code
  end

  def play
    system('clear')
    turns = NUMBER_OF_TURNS
    @guesses = []

    while turns > 0
      puts "You have #{turns} turns left."
      @guesses.push(self.get_guess)

      system('clear')
      self.display_matches(@guesses.last)

      if @secret_code.exact_matches(@guesses.last) == 4
        puts 'Congrats!'
        break
      elsif turns - 1 == 0
        puts 'So close! Try again.'
      end

      turns -= 1
    end
  end

  def get_guess
    puts 'Take a crack?: '
    guess = $stdin.gets.chomp

    Code.parse(guess)
  rescue
    puts 'Invalid guess, try again.'
    retry
  end

  def display_matches(guess)
    @guesses.each { |prev_guess| print "#{prev_guess.pegs}\n" }
    puts "exact: #{@secret_code.exact_matches(guess)}"
    puts "near: #{@secret_code.near_matches(guess)}"
  end
end
